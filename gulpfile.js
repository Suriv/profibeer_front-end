"use strict";

var gulp        = require('gulp'),
    // compass     = require('gulp-compass'),
    sass        = require("gulp-sass"),
    sourcemaps  = require("gulp-sourcemaps"),
    // notify      = require("gulp-notify"),
    autoprefixer= require('gulp-autoprefixer'),
    connect     = require("gulp-connect"),
    livereload  = require('gulp-livereload'),
    fileinclude = require('gulp-file-include'),
    concat      = require('gulp-concat'),
    order       = require('gulp-order'),
    gulpFilter  = require('gulp-filter'),
    mainBowerFiles = require('main-bower-files');

// Server connect
gulp.task('connect', function(cb) {
  connect.server({
    root: 'site',
    livereload: true
  });
  cb();
});

gulp.task('libs', function () {
  var jsFilter = gulpFilter('**/*.js',{restore: true});  //отбираем только  javascript файлы
  var cssFilter = gulpFilter('**/*.css',{restore: true});  //отбираем только css файлы
  return gulp.src(mainBowerFiles())
  // собираем js файлы , склеиваем и отправляем в нужную папку (в моем случае это www/js)
  .pipe(jsFilter)
  .pipe(order())
  // .pipe(concat('libs.js'))
  .pipe(gulp.dest('build/js/vendor/'))
  .pipe(jsFilter.restore)
  // собраем css файлы, склеиваем и отправляем их под синтаксисом scss
  .pipe(cssFilter)
  .pipe(order())
  .pipe(concat('_libs.scss')) //можно было оставить расширение css, но я использую sass, поэтому мне так удобнее
  .pipe(gulp.dest('build/scss/library/')); //в моем случае фаил lib.scss отправляется в папку dev/sass/library/ ,где дальше конкатенируеся с библиотекой , но это другая история
});

gulp.task('scss', function() {
  return gulp.src('build/scss/*.scss')
  .pipe(sourcemaps.init({loadMaps: true}))
  .pipe(sass().on('error', sass.logError))
  .pipe(autoprefixer({
      cascade: false
  }))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('build/css'))
  .pipe(gulp.dest('site/css'))
  .pipe(connect.reload());
});

// Autoprefixer
// gulp.task('autoprefixer', function() {
//   return gulp.src(['build/css/*.css'])
//     .pipe(autoprefixer({
//       cascade: false
//     }))
//     .pipe(gulp.dest('site/css/'))
//     .pipe(connect.reload());
//     // .pipe(notify(':)'));
// });

// Fileinclude
gulp.task('fileinclude', function() {
  return gulp.src(['build/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: 'build/template/'
    }))
    .pipe(gulp.dest('site/'));
});

// HTML
gulp.task('html', function() {
  return gulp.src('build/*.html')
    .pipe(connect.reload());
});

// JS
gulp.task('js', function() {
  return gulp.src('build/js/*.js')
    .pipe(gulp.dest('site/js/'))
    .pipe(connect.reload());
});

// Watch
gulp.task('watch', function (cb) {
    gulp.watch('build/scss/*.scss', gulp.parallel('scss'));
    // gulp.watch('build/css/*.css', gulp.parallel('autoprefixer'));
    gulp.watch('build/**/*.html', gulp.series('fileinclude', 'html'));
    gulp.watch('build/js/*.js', gulp.parallel('js'));
    gulp.watch('bower.json', gulp.parallel('libs'));
    cb();
});

// Default
gulp.task('default', gulp.series('connect', 'watch'));