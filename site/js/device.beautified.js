var device, documentElement, userAgent, find, addClass, hasClass, removeClass;

device = {}, window.device = device, documentElement = window.document.documentElement, 
userAgent = window.navigator.userAgent.toLowerCase(), find = function(e) {
    return -1 !== userAgent.indexOf(e);
}, device.iphone = function() {
    return !device.windows() && find("iphone");
}, device.ipod = function() {
    return find("ipod");
}, device.ipad = function() {
    return find("ipad");
}, device.android = function() {
    return !device.windows() && find("android");
}, device.androidPhone = function() {
    return device.android() && find("mobile");
}, device.androidTablet = function() {
    return device.android() && !find("mobile");
}, device.opera = function() {
    return find("opera");
}, device.operaMini = function() {
    return find("opera mini") && find("presto");
}, device.blackberry = function() {
    return find("blackberry") || find("bb10") || find("rim");
}, device.blackberryPhone = function() {
    return device.blackberry() && !find("tablet");
}, device.blackberryTablet = function() {
    return device.blackberry() && find("tablet");
}, device.windows = function() {
    return find("windows");
}, device.windowsPhone = function() {
    return device.windows() && find("phone");
}, device.windowsTablet = function() {
    return device.windows() && find("touch") && !device.windowsPhone();
}, device.fxos = function() {
    return (find("(mobile;") || find("(tablet;")) && find("; rv:");
}, device.fxosPhone = function() {
    return device.fxos() && find("mobile");
}, device.fxosTablet = function() {
    return device.fxos() && find("tablet");
}, device.meego = function() {
    return find("meego");
}, device.mobile = function() {
    return device.androidPhone() || device.iphone() || device.ipod() || device.windowsPhone() || device.blackberryPhone() || device.fxosPhone() || device.meego() || device.opera() || device.operaMini();
}, device.tablet = function() {
    return device.ipad() || device.androidTablet() || device.blackberryTablet() || device.windowsTablet() || device.fxosTablet();
}, device.portrait = function() {
    return window.innerHeight / window.innerWidth > 1;
}, device.landscape = function() {
    return window.innerHeight / window.innerWidth < 1;
}, hasClass = function(e) {
    var n;
    return n = new RegExp(e, "i"), documentElement.className.match(n);
}, addClass = function(e) {
    var n = null;
    hasClass(e) || (n = documentElement.className.replace(/^\s+|\s+$/g, ""), documentElement.className = n + " " + e);
}, removeClass = function(e) {
    hasClass(e) && (documentElement.className = documentElement.className.replace(" " + e, ""));
}, device.mobile() ? addClass("ismobile") : removeClass("ismobile");

device.tablet() ? addClass("istablet") : removeClass("istablet");