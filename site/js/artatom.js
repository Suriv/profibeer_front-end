	// function debounce(func, wait, immediate) {
	//   var timeout;
	//   return function() {
	//     var context = this, args = arguments;
	//     var later = function() {
	//       timeout = null;
	//       if (!immediate) func.apply(context, args);
	//     };
	//     var callNow = immediate && !timeout;
	//     clearTimeout(timeout);
	//     timeout = setTimeout(later, wait);
	//     if (callNow) func.apply(context, args);
	//   };
	// };

	// var resizeFn = debounce(function() {

	// 	$(".tab-content-wrap").css({ height: $(".tab-content-el.active").height() });

	// }, 100);

	// var supportsCSS = !!((window.CSS && window.CSS.supports) || window.supportsCSS || false);
	// 	console.log( CSS.supports("::-webkit-scrollbar") );

	// Loading Begin 130311 v10 S >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		$(
			function() {

				// datapicker settings
				if ($.datepicker) {
					$.datepicker.regional['ru'] = {
				      closeText: 'Закрыть',
				      prevText: '&#x3c;Пред',
				      nextText: 'След&#x3e;',
				      currentText: 'Сегодня',
				      monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
				      'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
				      monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
				      'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
				      dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
				      dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
				      dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
				      weekHeader: 'Нед',
				      dateFormat: 'dd.mm.yy',
				      firstDay: 1,
				      isRTL: false,
				      showMonthAfterYear: false,
				      yearSuffix: ''
				  };
				  $.datepicker.setDefaults($.datepicker.regional['ru']);

					$(".ui-datapicker_search-from").datepicker({
							dateFormat: "dd.mm.yy",
				      language: 'ru',
//							maxDate: new Date(),
							onClose: function( selectedDate ) {
			        	$(this).closest(".date-input").find( ".ui-datapicker_search-to" ).datepicker( "option", "minDate", selectedDate );
			        }
						}, $.datepicker.regional['ru'] );

					$(".ui-datapicker_search-to").datepicker({
							dateFormat: "dd.mm.yy",
				      language: 'ru',
//							maxDate: new Date(),
							onClose: function( selectedDate ) {
			        	$(this).closest(".date-input").find( ".ui-datapicker_search-from" ).datepicker( "option", "maxDate", selectedDate );
			        }
						}, $.datepicker.regional['ru'] );

						// $(".ui-datapicker_year").each( function(){
						// 	$(this).datepicker({
						// 		dateFormat: "dd.mm.yy",
					 //      language: 'ru',
					 //      yearRange: "1940:2016",
					 //      changeYear: true
						// 	}, $.datepicker.regional['ru'] );
						// });

				}
				// datapicker settings end

				// init for niceselect
					$('select').niceSelect();

				// init for photobox on product page
				if(	$('.js-gal').length >= 1 ){
					$('.js-gal').photobox('.js-gal__el',{time:0,zoomable:false});
				}

			}
		);
	// Loading End >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


;(function() {
  // glob variable
	HTML   = document.documentElement;
	HEAD   = document.head;
	BURGER = document.querySelector(".nav-top-burger");
	SEARCH_OPEN = document.querySelector(".nav-top-search-btn_toggle");

	svg4everybody(); // подгруджается в head
	swiperInit(); // Swiper initialize

	function lockDocument() {
		HTML.classList.add('lock');
	};
	function unlockDocument() {
		HTML.classList.remove('lock');
	};

	// close menu global
	function mobMenuClose() {
		BURGER.classList.remove('open');
		HTML.classList.remove('mob-menu_open','lock');
	}
	window.mobMenuClose = mobMenuClose;

	// open close menu and close modal
	burger();
	function burger() {
		BURGER.addEventListener("click", function( e) {
			e.preventDefault();
			SEARCH_OPEN.classList.remove('open');
			if ( this.getAttribute("data-open") == "modal" ) {

				var g = document.querySelector(".modal.open .modal__wrap");
				modalClose(g);
			} else if ( this.getAttribute("data-open") == "menu" ) {
				this.classList.toggle('open');
				HTML.classList.toggle('mob-menu_open');
				HTML.classList.toggle('lock');
			}
		});
	}

	// Close mob menu  on click on scroll
	$(".nav-r-el[data-scroll]").on("click", mobMenuClose);
	// close mob menu  on click on scroll

	// open close search for some relolution
	searchHeader();
	function searchHeader() {
		SEARCH_OPEN.addEventListener( "click", function(e) {
			HTML.classList.remove('mob-menu_open','lock');
			BURGER.classList.remove('open');
			this.classList.toggle('open');
			var g = document.querySelector(".modal.open .modal__wrap");
			if (g !=null) {
				modalClose(g);
			}
			if( !this.classList.contains("open") ){
				e.preventDefault();
			}
		});
	};

	// modal ctrl
		if (document.querySelector(".modal__wrap")) {
			document.querySelector(".modal__wrap").addEventListener("click", function(e){
				if ( e.target.getAttribute("data-close") == "" ) {
					modalClose(this);
				}
			});
		}

		initModal = document.querySelectorAll("[data-modal]");
		for (var i = initModal.length - 1; i >= 0; i--) {
			initModal[i].addEventListener("click", function( e) {
				e.preventDefault();
				id = this.getAttribute("href");
				$(id).trigger('beforeOpen'); //***
				document.querySelector(id).classList.remove("hide");
				document.querySelector(id).classList.add("open");
				lockDocument();
				BURGER.setAttribute("data-open", "modal");
				BURGER.classList.add('open');
				SEARCH_OPEN.classList.remove('open');
				$(id).trigger('afterOpen'); //***
			});
		}

		function modalClose(o, e) {
			var this_ = 	o.closest(".modal"),
					computedStyle = getComputedStyle(this_),
					time = computedStyle.transitionDuration,
					time = parseFloat(time) * 1000;

			BURGER.setAttribute("data-open", "menu");
			BURGER.classList.remove('open');
			this_.classList.add("hide");
			this_.classList.remove("open");
			setTimeout( unlockDocument, time);
		};
		window.modalClose = modalClose;
	// modal ctrl

	// Smothscroll initialize >>>>>>
	initSubHeader();
	function initSubHeader() {
		smoothScroll.init({
			selectorHeader: '.nav-top',
      speed: 400,
      offset: 0,
      callback: function(anchor, toggle ) {
      	// focus on first .input in subscribe
      	if ( anchor.id === 'footer-subscribe' ) {
	      	document.querySelector("#footer-subscribe .input").focus();
      	}
      }
    });
	}

	// Replace SVG b 151016 v10 A >>>>>>>>
	replaceSVG();
	function replaceSVG() {
		if ((! window.SVGAngle)) {
			img = document.querySelectorAll("img[data-png]");
			for (var i = img.length - 1; i >= 0; i--) {
				img[i].src = img[i].dataset.png;
			}
		}
	};

	// link tel dependece device.js & style_diz.css
	if ( (!device.mobile()) && (!device.tablet()) ) {

		var _el = document.querySelectorAll(".js-tel"),
				_elLength = _el.length;

		if (_elLength) {
			for (var i = _elLength - 1; i >= 0; i--) {
				_el[i].addEventListener("click", function(event){
					event.preventDefault();
				});
			}
		};
	};

  // add palecholder from cdn if this is not support
  placeholder();
  function placeholder() {
    var dummy = document.createElement('input');
    if ("placeholder" in dummy) {
    } else {
      document.addEventListener("DOMContentLoaded", function(){
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.async = true;
        script.src = "https://cdnjs.cloudflare.com/ajax/libs/placeholders/4.0.1/placeholders.min.js";
        HEAD.appendChild(script);
        HTML.classList.add('no-placeholder');
      });
    }
  }

	// reset for select in search page on today
	jsReset();
	function jsReset() {
		var reset = document.querySelector('.js-reset');
		if (reset != null){
			reset.addEventListener("click", function() {
				var parent = 	this.closest("form"),
						select = parent.querySelectorAll("select");
				for (var i = select.length - 1; i >= 0; i--) {
					select[i].options.selectedIndex = 0;
					$(select[i]).niceSelect('update');
				}
			});
		}
	}

	// fastclick initialize
	if ('addEventListener' in document) {
    document.addEventListener('DOMContentLoaded', function() {
        FastClick.attach(document.body);
    }, false);
	}

	// Swiper slider initialize  >>>>>>>>
	function swiperInit() {
		// big slider for about_beer_in page >>>>>>>
		var sliderIntext = $(".slider_intext-container_slider");
		sliderIntext.each(function(){
			var $this = $(this),
					sliderIntextCont = $this.find(".slider_intext"),
					sliderIntextNext = $this.find(".slider_intext-btn_next"),
					sliderIntextPrev = $this.find(".slider_intext-btn_prev"),
					sliderIntextPag = $this.find(".slider_intext__pagination"),
					swiper = new Swiper(sliderIntextCont, {
	          nextButton: sliderIntextNext,
	          prevButton: sliderIntextPrev,
	          pagination: sliderIntextPag,
	        	paginationType: 'fraction',
	          simulateTouch: false,
				    // Disable preloading of all images
				    preloadImages: false,
				    // Enable lazy loading
				    lazyLoading: true
	      	});
    	});

		// big slider for all_beer-in page >>>>>>>
		var swiper = new Swiper('.all_beer-slider', {
          nextButton: '.all_beer-slider-btn_next',
          prevButton: '.all_beer-slider-btn_prev',
          simulateTouch: false,
			    // Disable preloading of all images
			    preloadImages: false,
			    // Enable lazy loading
			    lazyLoading: true
      	});
		// initialize for button >>>>>>>
		if ( document.querySelectorAll(".all_beer-slide").length > 1 ) {
			document.querySelector(".all_beer-slider").classList.add("btn-active");
		}


		// banner slider for all pages >>>>>>>
		var bannerCont = $(".g-banner-h__cont");
		var banners = $(".js-banner-slider");
		banners.each( function(index, element){
			var _time = (10000 + 1000*index);
			var $this = $(this);
			var swiperBanner = new Swiper($this, {
	          	simulateTouch: false,
			    // Disable preloading of all images
			    preloadImages: false,
			    // Enable lazy loading
			    lazyLoading: true,
			    // loop: true,
			    autoplay: _time,
			    disableOnInteraction: false,
			    effect: "fade",
			    fade: {
					crossFade: true
				},
			    speed: 1000
	      	});
	      	swiperBanner.once("onSetTranslate", function(e){
	      		e.params.autoplay = 5000;
	      		e.autoplaying = true;
      			// console.log(e);
	      	});
		});
	}
	// / Swiper slider initialize e >>>>>>>>>>>>>>>>>>>>>>>

	// filter btn toggle for mobile
	filter();
	function filter() {
		var cont = document.querySelector(".js-filter__cont");

		if (cont != null ) {
			var btn = cont.querySelector(".js-filter__btn"),
					wrap = cont.querySelector(".js-filter__wrap"),
					wrapIn = cont.querySelector(".js-filter__wrap-in");

			btn.addEventListener("click", function() {

				function wrapHeight() {
					wrap.style.height = wrapIn.offsetHeight + "px";
				}

				if ( wrap.offsetHeight == 0 ) {
					wrap.style.height = wrapIn.offsetHeight + "px";
					setTimeout(function(){ cont.classList.add("open"); }, 450);
					window.addEventListener("resize", function() {
						if (wrap.offsetHeight !== 0){ wrapHeight();	}
					}, false);
				} else {
					cont.classList.remove("open");
					setTimeout(function(){ wrap.style.height = ""; }, 100);
				}
			});

			var btn_abc = cont.querySelectorAll(".filter-abc-mtabs__btn");
			for (var i = btn_abc.length - 1; i >= 0; i--) {
				btn_abc[i].addEventListener("click", function(e) {
					e.preventDefault();
					cont.querySelector("[data-abc-ctrl]").removeAttribute("data-abc-ctrl");
					this.setAttribute("data-abc-ctrl", true);

					var href = this.getAttribute("href");
					cont.querySelector("[data-abc-content]").removeAttribute("data-abc-content");
					cont.querySelector(href).setAttribute("data-abc-content", true);
				})
			}
		}
	}
	// / filter btn toggle for mobile

	acordion();
	function acordion() {
		$(".ac-item.active").each( function() {
			$(this).find(".ac-item__desc").height( $(this).find(".ac-item__desc-in").innerHeight() );
		});

		$(".ac-item__title").click( function(){
			if ($(this).closest(".ac-item").hasClass("active")) {
				$(this).closest(".ac-item").removeClass("active");
				cont = $(this).closest(".ac-item").find(".ac-item__desc");
				cont.height(0);
			} else {
				$(this).closest(".ac-item").addClass("active");
				cont = $(this).closest(".ac-item").find(".ac-item__desc");
				cont.height(cont.find(".ac-item__desc-in").innerHeight());
			}
		});

		// $(window).resize( function(){
		// 	cont.height(cont.find(".ac-item__desc-in").innerHeight());
		// });
	};

	// readmore for lawyer page
	window.onload = readMore;
	function readMore() {
		var item = document.querySelectorAll(".lawyer-cons-pos");

		for (var i = item.length - 1; i >= 0; i--) {
			var p_first = item[i].querySelector(".lawyer-cons-pos__answer .editor p"),
				wrap = item[i].querySelector(".lawyer-cons-pos__answer-wrap");
			// first initialize
			if ( p_first ) {
				wrap.style.maxHeight = p_first.offsetHeight + "px";
			}

			// var more = item[i].querySelector(".lawyer-cons-pos__link");
			// more && more.addEventListener("click", function(e) {
			// 	var parent = this.parentElement,
			// 			p_first = parent.querySelector(".lawyer-cons-pos__answer .editor p"),
			// 			wrap = parent.querySelector(".lawyer-cons-pos__answer-wrap"),
			// 			p_fullHeight = parent.querySelector(".lawyer-cons-pos__answer .editor").offsetHeight;
			// 	e.preventDefault();
			// 	this.classList.toggle("open");
			// 	// alert(this.classList.contains("open"));
			// 	if ( p_first ) {
			// 		if ( this.classList.contains("open") ) {
			// 			wrap.style.maxHeight = p_fullHeight + "px";
			// 		} else {
			// 			wrap.style.maxHeight = p_first.offsetHeight + "px";
			// 		}

			// 	} else {

			// 		if ( this.classList.contains("open") ) {
			// 			wrap.style.maxHeight = p_fullHeight + "px";
			// 		} else {
			// 			wrap.style.maxHeight = "";
			// 		}
			// 	}
					// });
		}

		// setTimeout(function(){

		// 	for (var i = item.length - 1; i >= 0; i--) {
		// 		var p_first = item[i].querySelector(".lawyer-cons-pos__answer .editor p"),
		// 				wrap = item[i].querySelector(".lawyer-cons-pos__answer-wrap");
		// 				wrap.classList.add("transition");
		// 	}

		// }, 100);
	}

	// show message in subscribe block
	showMessage();
	function showMessage() {
		var btn = document.querySelector(".js-subscribe-btn");
		if ( btn != null ) {
			btn.addEventListener("click", function(e) {
				e.preventDefault();
				this.closest(".g-subscribe-form").classList.add("hide_field");
			})
		}
	}

	// add input file (use in cab marketing and reques pages)
	(function(){
	  	var $cont = $('.g-upload'),
	      	maxCount = 20,
	      	cl = function(){ return $('<div class="g-upload__in">\
	                  <div class="g-upload-btn g-vac">\
                        <svg role="presentation" class="g-upload-btn__ico">\
                          <use xlink:href="images/sprite.svg#symbol-upload"></use>\
                        </svg>\
                      <span class="g-upload-btn__in g-vac__el">\
                        <span class="g-upload-btn__txt f_stm">Загрузить файлы</span>\
                        <span class="g-upload-btn__txt_dop f_str">(не более 5 мб каждый)</span>\
                      </span>\
                      <input type="file" class="g-upload-btn__input" placeholder="" value="" />\
	                  </div>\
	                  <div class="g-upload-file">\
	                  	<div class="g-upload-file__in">\
		                    <span class="g-upload-file__name"></span>\
		                    <button type="button" class="g-upload-file__del abs abs_r">\
		                      <svg role="presentation" class="g-upload-file__del-ico abs">\
		                        <use xlink:href="images/sprite.svg#symbol-delet"></use>\
		                      </svg>\
		                    </button>\
		                  </div>\
		                </div>\
	                </div>')};
  		$cont.on("change", '.g-upload-btn__input', function () {
		    var file = this.files, //Files[0] = 1st file
		        $this = $(this),
		        el = $this.closest(".g-upload__in"),
		        elName = el.find(".g-upload-file__name"),
		        clNew = cl();

		    if (this.value != '' ) {
		      	if ( $cont.find(".g-upload__in").length < maxCount ){
		        	clNew.appendTo($cont);
		      	}
		      	if (file != undefined ){
			        el.addClass("active");
			        elName.text(file[0].name);
			    } else {
			        el.addClass("active");
			        elName.text(this.value.slice(this.value.lastIndexOf("\\") + 1));
			    }
		    }
		});

	  	$('.g-upload').on("click", ".g-upload-file__del", function(){
		    var $this = $(this);
		    $this.closest(".g-upload__in").remove();
		    if ( $cont.find(".g-upload__in.active").length == maxCount-1 ){
		      	var clNew = cl();
		      	clNew.appendTo($cont);
		    }
	  	});
	}());

	// Comments form enter show and hide
	$('.comments-checkin').each(function(){
		var _this = $(this),
			_open = _this.find(".js-comments-checkin__form"),
			_close = _this.find(".js-comments-checkin__not");
		_open.on('click', function(e){
			e.preventDefault();
			_this.toggleClass("form-active");
		});
		_close.on('click', function(e){
			e.preventDefault();
			_this.toggleClass("form-active");
		});
	});
	// comments form enter show and hide

	// Show & hide notice in header
		(function(){
			"use strict"
			const _closeNotice = (event) => {
				const _target = event.target;
				if (
					!_target.closest('.js-header-notice') ||
					event.code === "Escape"
				) {
					document.removeEventListener("keydown", _closeNotice, false);
					document.removeEventListener("click", _closeNotice, false);
					noticeCont.classList.remove("is-open");
				}
			};
			const noticeCont = document.querySelector('.js-header-notice');
			if( noticeCont ) {
				const noticeBtn = noticeCont.querySelector('.js-header-notice__btn');
				const userId = noticeCont.getAttribute('data-id');
				noticeBtn && noticeBtn.addEventListener("click", (event) => {
					if ( noticeCont.classList.contains("is-open") ) {
						noticeCont.classList.remove("is-open");
					} else {
						noticeCont.classList.add("is-open");
                        noticeCont.classList.add("is-read");
						document.addEventListener("keydown", _closeNotice, false);
						document.addEventListener("click", _closeNotice, false);
						$.ajax({
							url: '/users/ajax/',
							method: 'POST',
							cache: false,
							dataType: 'text',
							data: `action=markNotificationsViewed&siteuser=${userId}`,
							success: function(data){
								console.log(data);
							},
				            error: function(data) {
				                console.log(data);
				            }
						});
					}
				});
			}
		}());
	// show & hide notice in header

	window.addEventListener('DOMContentLoaded', function(){
		const footer = document.querySelector(".g-footer");
		if (footer) {
		    const footerOffsetTop = footer.offsetTop - window.innerHeight;
		    function scrollY() { return window.pageYOffset || docElem.scrollTop; }
		    if(window.scrollY > footerOffsetTop) {
		        document.querySelector(".js-to-top").classList.add('is-stick');
		    }
		    window.onscroll = function() {
		    		console.log( window.scrollY )
		        if(window.scrollY > footerOffsetTop) {
		            document.querySelector(".js-to-top").classList.add('is-stick');
		        }
		        if(window.scrollY < footerOffsetTop) {
		            document.querySelector(".js-to-top").classList.remove('is-stick');
		        }
		    };
		}
	});
}());
