/*
 * aa-form-verification.js
 *
 * Верификация форм
 *
 * зависимости:
 *    /assets/js/jquery.validate.js
 *    /assets/js/localization/messages_ru.min.js
 *    /assets/js/jquery.form.min.js
 *
 */

$(function(){
	// Привязка к кнопкам отправки форм, у которых нет собственного обработчика
	// onclick() стандартного обработчика плагина jquery.form
	// $("input[type='submit']:not([onclick]), button[type='submit']:not([onclick])").on("click", function(){
	// 	$(this).closest('form').submit();
	// 	return false;
	// });

	// Установка дефолтовых значений валидатора
	$.validator.setDefaults({
		// debug: true,
		submitHandler: function(form) {
			$.loadingScreen('show');
			$form = $(form);
			$form.ajaxSubmit({ success: ajaxPopupFormSuccessDefault, dataType: 'json', type: 'post' });
		},
		ignore: [],
		focusInvalid: true,
		highlight: function(element, errorClass, validClass) {
			e = $(element);
			a = e.attr("placeholder");
			a = typeof a === 'undefined' ? '' : a;
			if( typeof e.data("placeholder_save") === 'undefined' )
			{
				e.data("placeholder_save", a);
				e.attr("placeholder", e.data("placeholder_save") + "..?").addClass("error");
			}
		},
		unhighlight: function(element, errorClass, validClass) {
			e = $(element);
			if( typeof e.data("placeholder_save") !== 'undefined' )
			{
				e.attr("placeholder", e.data("placeholder_save")).removeClass("error");
			}
		},
		errorPlacement: function(error, element) {
			return;
		}
	});

	function ajaxPopupFormSuccessDefault(json, statusText, xhr, form) {
		$.loadingScreen('hide');
		$form = $(form);
		$wrapper = $form.closest('.js-box_popup');
		$wrapper_form = $form.closest(".js-box-form-wrap_f");
		$wrapper_cancel = $wrapper.find('.js-box_popup-cancel');
		$wrapper_message = $wrapper.find('.js-box-form-wrap__message');
		$wrapper_message_title = $wrapper_message.find('.box-form-title span');
		$wrapper_message_text  = $wrapper_message.find('.box-form-in div');

		$wrapper_form.removeClass('active');
		$wrapper_message.addClass('active')
		$wrapper_message_title.html(json.title);
		$wrapper_message_text.html(json.error.length == 0 ? json.msg : json.error);

		console.log(json.msg);

		setTimeout( function() {
			$wrapper_form.addClass('active');
			$wrapper_message.removeClass('active');
			$wrapper_message_title.html('');
			$wrapper_message_text.html('');
			// сбрасываем поля формы
			if ( json.error.length == 0 )
			{
				$wrapper_cancel.trigger('click');
				$form.validate().resetForm();
			}
		}, 3000);
	}

	//** Правила валидации **//

	// Валидация формы "Написать свой комментарий к товару"
	$("#form_auth").validate({
		// Свой обработчик сабмита т.к. при добавлении комментария к товару
		// требуется аттачить id самого товара (не модификации!)
		submitHandler: function(form) {
			$.loadingScreen('show');
			$form = $(form);
			// $form.ajaxSubmit({ success: ajaxPopupFormSuccessDefault, dataType: 'json', type: 'post' });
		},
		rules: {
			telandmail: {
				required: true,
				minlength: 2
			},
			// опыт использования
			password: {
				required: true,
				minlength: 2
			},
			// email: {
			// 	required: true,
			// 	email: true
			// },
			// text: {
			// 	required: true,
			// 	minlength: 3,
			// 	maxlength: 500
			// },
			// captcha: {
			// 	required: true,
			// 	minlength: 4,
			// 	maxlength: 4,
			// 	digits: true
			// }
		}
	});

	// Валидация формы "Быстрый заказ"
	$("form[name='fast_order']").validate({
		// Берем ID модификации (если есть), иначе ID самого товара
		submitHandler: function(form) {
			$.loadingScreen('show');
			$form = $(form);
			var shop_item_id = $('.g-prod-color:not(:hidden) a.active').data('product-id');
			if (typeof shop_item_id === 'undefined') {
				shop_item_id = $('section.p-prod').data('product-id');
			}
			$form.find("input[name = 'shop_item_id']").val(shop_item_id);
			$form.ajaxSubmit({ success: ajaxPopupFormSuccessDefault, dataType: 'json', type: 'post' });
		},
		rules: {
			name: {
				required: true,
				minlength: 2
			},
			phone: {
				required: true,
				minlength: 7,
				maxlength: 11, // 7 - городской, 11 - мобильный (89207654321)
				digits: true
			},
		}
	});

	// Валидация формы "Регистрация дистрибьютера"
	$("form[name='reg_distributer']").validate({
		rules: {
			name: {
				required: true,
				minlength: 2
			},
			firm: {
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			phone: {
				minlength: 7,
				maxlength: 11, // 7 - городской, 11 - мобильный (89207654321)
				digits: true
			},
		}
	});

});
