$(function() {

  // polifill for closest
  (function(ELEMENT) {
    ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
    ELEMENT.closest = ELEMENT.closest || function closest(selector) {
        if (!this) return null;
        if (this.matches(selector)) return this;
        if (!this.parentElement) {return null}
        else return this.parentElement.closest(selector)
      };
  }(Element.prototype));

	// Sticky sidebar initial >>>>>>
		$('.g-cont .g-layout_l, .g-cont .g-layout_r').theiaStickySidebar({
      // Settings
      additionalMarginTop: 64
    });

  // Picture element HTML shim|v it for old IE (pairs with Picturefill.js)
  document.createElement( "picture" );

});
